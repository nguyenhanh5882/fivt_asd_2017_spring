#include <iostream>
#include <string>
#include <vector>

using namespace std;

class ZFunction {
public:
	ZFunction(string _text, string _pattern) : text(_text), pattern(_pattern) {}
	//~ZFunction (){};
	vector<int> matchPattern();
	void display(vector<int> result) {
		for (int j = 0; j < result.size(); j++) {
			cout << result[j] << " ";
		}
	}
private:
	string pattern;
	string text;
	vector<int> calZ(string input);

};

vector<int> ZFunction::calZ(string input) {
	vector<int> Z(input.length());
	int left(0);
	int right(0);
	for (int i = 1; i < input.length(); i++) {
		if (i > right) {
			left = right = i;
			while (right < input.length() && input[right] == input[right - left])
				right++;
			Z[i] = right - left;
			right--;
		}
		else {
			int j = i - left;
			if (Z[j] < right - i - 1) {
				Z[i] == Z[j];
			}
			else {
				left = i;
				while (right < input.length() && input[right] == input[right - left]) {
					right++;
				}
				Z[i] = right - left;
				right--;
			}
		}


	}
	return Z;
}

vector<int> ZFunction::matchPattern() {
	string new_text;
	int i = 0;
	new_text = pattern + '$' + text;

	vector<int> result;
	vector<int> Z = calZ(new_text);
	for (int i = 0; i < Z.size(); i++) {
		if (Z[i] == pattern.length()) {
			result.push_back(i - pattern.length() - 1);
		}
	}
	return result;
}

int main()
{
	string pattern;
	string text;
	cin >> pattern >> text;
	ZFunction A(text, pattern);
	A.display(A.matchPattern());
	//system("pause");
	return 0;
}