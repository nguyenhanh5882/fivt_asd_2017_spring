#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class zfunction {
public:
	zfunction(vector<int> _input) : input(_input) {}
	~zfunction(){};
	void calz();
private:
	vector<int> input;
	vector<int> z_func_to_pre_func(vector<int> input);
};

void zfunction::calz() {
	string text;
	char insert = 'a';
	vector<int> pre = z_func_to_pre_func(input);
	text += 'a';
	for (int i = 1; i < pre.size(); i++) {
		if (pre[i] != 0) {
			text += text[pre[i] - 1];
		}
		else {
			vector<bool> diff_char(text.length(), false);
			diff_char[0] = true;
			int k = i;
			while (k > 0) {
				k = pre[k - 1];
				diff_char[text[k] - 'a'] = true;
			}
			bool full = true;
			for (int j = 0; j < diff_char.size(); j++) {
				if (!diff_char[j]) {
					char c = (char)('a' + j);
					text = text + c;
					full = false;
					break;
				}
			}
			if (full) {
				char c = (char)('a' + diff_char.size());
				text = text + c;
				full = false;
			}
		}
	}
	cout << text;
}

vector<int> zfunction::z_func_to_pre_func(vector<int> input) {
	vector<int> pre(input.size());
	for (int i = 1; i < input.size(); i++) {
		pre[i + input[i] - 1] = max(pre[i + input[i] - 1], input[i]);
	}
	for (int i = input.size() - 2; i >= 0; --i) {
		pre[i] = max(pre[i + 1] - 1, pre[i]);
	}
	return pre;
}



int main()
{
	vector<int> input;
	int number;
	while (true) {
		if (!(cin >> number)) break;
		input.push_back(number);
	}
	zfunction A(input);
	A.calz();
	//system("pause");
	return 0;
}
