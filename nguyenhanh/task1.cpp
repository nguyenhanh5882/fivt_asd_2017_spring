#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class zfunction {
public:
	zfunction(vector<int> _input) : input(_input) {}
	~zfunction(){};
	void calz();
private:
	vector<int> input;
};

void zfunction::calz() {
	string text;
	text += 'a';
	for (int i = 1; i < input.size(); i++) {
		if (input[i] != 0) {
			text += text[input[i] - 1];
		}
		else {
			vector<bool> diff_char(text.length(), false);
			diff_char[0] = true;
			int k = i;
			while (k > 0) {
				k = input[k - 1];
				diff_char[text[k] - 'a'] = true;
			}
			bool full = true;
			for (int j = 0; j < diff_char.size(); j++) {
				if (!diff_char[j]) {
					char c = (char)('a' + j);
					text = text + c;
					full = false;
					break;
				}
			}
			if (full) {
				char c = (char)('a' + diff_char.size());
				text = text + c;
				full = false;
			}
		}
	}
	cout << text;
}




int main()
{
	vector<int> input;
	int number;
	while (true) {
		if (!(cin >> number)) break;
		input.push_back(number);
	}
	zfunction A(input);
	A.calz();
	return 0;
}
